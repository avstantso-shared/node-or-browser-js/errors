import { JS } from '@avstantso/node-or-browser-js--utils';

import { BaseError } from './BaseError';

export class AbstractError extends BaseError {
  constructor(message: string = 'Abstract error', internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, AbstractError.prototype);
  }

  static is(error: unknown): error is AbstractError {
    return JS.is.error(this, error);
  }
}
