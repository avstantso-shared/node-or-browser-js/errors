import { JS } from '@avstantso/node-or-browser-js--utils';

const stackToArr = (error: Error): string[] => {
  const r = error.stack.split('\n').map((s) => s.trim());

  const cut = (pattern: string, replacement: string): void => {
    const i = r.findIndex((s) => s.includes(pattern));
    r.splice(i, r.length - i, replacement);
  };

  // Дальнейшая информация занимает много места, а толку в ней нет. Сократим...
  cut('bluebird-npm', '...bluebird');

  return r;
};

export class BaseError extends Error {
  internal: Error;

  constructor(message: string, internal?: Error) {
    super();
    this.message = message;
    if (undefined !== internal) this.internal = internal;

    Object.setPrototypeOf(this, BaseError.prototype);
  }

  static is(error: unknown): error is BaseError {
    return JS.is.error(this, error);
  }

  static toLogAnyError(error: Error, parentMessage?: string): object {
    return {
      class: error.constructor.name,
      ...(!parentMessage || parentMessage != error.message
        ? { message: error.message }
        : {}),
      details: {
        stack: stackToArr(error),
      },
    };
  }

  protected toLogInternal(): object {
    if (!this.internal) return undefined;

    if (BaseError.is(this.internal))
      return (this.internal as BaseError).toLog(this.message);

    return BaseError.toLogAnyError(this.internal, this.message);
  }

  protected toLogDetails(): object {
    return {
      stack: stackToArr(this),
      ...(this.internal ? { internal: this.toLogInternal() } : {}),
    };
  }

  public toLog(parentMessage?: string): object {
    return {
      class: this.constructor.name,
      ...(!parentMessage || parentMessage != this.message
        ? { message: this.message }
        : {}),
      details: this.toLogDetails(),
    };
  }
}
