import { JS } from '@avstantso/node-or-browser-js--utils';

import { BaseError } from './BaseError';

/**
 * @summary Handle cases imposible (or imposible-like)
 * @example
 * const a = 1;
 * if (1 === a)
 *   console.log('Positive')
 * else if (1 !== a)
 *   console.log('Negative')
 * else
 *   throw new ImposibleCaseError();
 */
export class ImposibleCaseError extends BaseError {
  constructor(message: string = 'Imposible case error', internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, ImposibleCaseError.prototype);
  }

  static is(error: unknown): error is ImposibleCaseError {
    return JS.is.error(this, error);
  }
}
