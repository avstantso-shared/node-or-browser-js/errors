import { JS } from '@avstantso/node-or-browser-js--utils';

import { BaseError } from './BaseError';

export class InvalidArgumentError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);
  }

  static is(error: unknown): error is InvalidArgumentError {
    return JS.is.error(this, error);
  }
}
