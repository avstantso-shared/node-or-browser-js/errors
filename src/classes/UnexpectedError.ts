import { JS } from '@avstantso/node-or-browser-js--utils';

import { BaseError } from './BaseError';

export class UnexpectedError extends BaseError {
  constructor(message: string = 'Unexpected error', internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, UnexpectedError.prototype);
  }

  static is(error: unknown): error is UnexpectedError {
    return JS.is.error(this, error);
  }
}
