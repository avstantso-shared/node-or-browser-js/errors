export namespace Details {
  export type Union = Details | any[];
  export type Provider = { details: Details.Union };
}

export interface Details {
  [key: string]: any;
}
